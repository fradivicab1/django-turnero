from django.urls import path
from .views import Tasklist, CreateTask

urlpatterns = [
    path('',Tasklist.as_view(), name='tasks'),
    path('create/', CreateTask.as_view(), name='create-task'),
]